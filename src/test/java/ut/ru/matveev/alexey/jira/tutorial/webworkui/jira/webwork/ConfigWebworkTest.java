package ut.ru.matveev.alexey.jira.tutorial.webworkui.jira.webwork;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ru.matveev.alexey.jira.tutorial.webworkui.jira.webwork.ConfigWebwork;

import static org.mockito.Mockito.*;

/**
 * @since 3.5
 */
public class ConfigWebworkTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test(expected=Exception.class)
    public void testSomething() throws Exception {

        //ConfigWebwork testClass = new ConfigWebwork();

        throw new Exception("ConfigWebwork has no tests!");

    }

}
