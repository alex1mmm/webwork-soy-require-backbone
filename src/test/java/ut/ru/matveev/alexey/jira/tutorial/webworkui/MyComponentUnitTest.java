package ut.ru.matveev.alexey.jira.tutorial.webworkui;

import org.junit.Test;
import ru.matveev.alexey.jira.tutorial.webworkui.api.MyPluginComponent;
import ru.matveev.alexey.jira.tutorial.webworkui.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}