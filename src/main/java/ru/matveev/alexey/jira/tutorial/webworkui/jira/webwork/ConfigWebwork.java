package ru.matveev.alexey.jira.tutorial.webworkui.jira.webwork;

import com.atlassian.jira.web.action.ActionViewData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import ru.matveev.alexey.jira.tutorial.webworkui.api.PluginSettingService;

import javax.inject.Inject;

public class ConfigWebwork extends JiraWebActionSupport
{
    private static final Logger log = LoggerFactory.getLogger(ConfigWebwork.class);
    private final PluginSettingService pluginSettingService;
    private String configJson;

    @Inject
    public ConfigWebwork(PluginSettingService pluginSettingService) {
        this.pluginSettingService = pluginSettingService;
    }

    @Override
    public String execute() throws Exception {
        super.execute();
        return SUCCESS;
    }

    public void doSave() {
        pluginSettingService.setConfigJson(configJson);
    }

    @ActionViewData
    public String getConfigJson() {
        return pluginSettingService.getConfigJson().isEmpty()?"{}":pluginSettingService.getConfigJson();
    }

    public void setConfigJson(String json) {
        this.configJson = json;
    }

}
