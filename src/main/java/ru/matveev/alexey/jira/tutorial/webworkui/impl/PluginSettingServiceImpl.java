package ru.matveev.alexey.jira.tutorial.webworkui.impl;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import ru.matveev.alexey.jira.tutorial.webworkui.api.PluginSettingService;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class PluginSettingServiceImpl implements PluginSettingService {

    public final PluginSettings pluginSettings;
    private static final String PLUGIN_STORAGE_KEY = "ru.matveev.alexey.jira.tutorial.webworkui.";
    private static final String CONFIG_JSON = "configjson";


    @Inject
    public PluginSettingServiceImpl(@ComponentImport PluginSettingsFactory pluginSettingsFactory) {
        this.pluginSettings = pluginSettingsFactory.createGlobalSettings();

    }

    private void setSettingValue(String settingKey, String settingValue) {
            this.pluginSettings.put(PLUGIN_STORAGE_KEY + settingKey, settingValue != null?settingValue:"");
    }

    private String getSettingValue(String settingKey) {
            return pluginSettings.get(PLUGIN_STORAGE_KEY + settingKey) != null?pluginSettings.get(PLUGIN_STORAGE_KEY + settingKey).toString():"";
    }

    @Override
    public String getConfigJson() {
        return getSettingValue(CONFIG_JSON);
    }

    @Override
    public void setConfigJson(String json) {
        setSettingValue(CONFIG_JSON, json);
    }
}

