package ru.matveev.alexey.jira.tutorial.webworkui.api;

public interface PluginSettingService {
    String getConfigJson();
    void setConfigJson(String json);
}
