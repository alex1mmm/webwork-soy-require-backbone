package ru.matveev.alexey.jira.tutorial.webworkui.api;

public interface MyPluginComponent
{
    String getName();
}