define('webwork/config/model', [
    'jquery',
    'backbone',
    'underscore'
], function($, Backbone, _) {
    var WebConfigModel = Backbone.Model.extend({
        defaults: {
          parameter1: '',
          parameter2: '',
          parameter3: '',
          parameter4: ''
         }
    });
    return {
              Model: WebConfigModel
    };
})