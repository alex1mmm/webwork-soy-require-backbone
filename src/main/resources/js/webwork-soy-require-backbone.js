 require([
            'webwork/config/view',
            'webwork/config/model',
            'jquery',
            'backbone',
            'underscore'
        ], function(webworkConfigView, webworkConfigModel, $, Backbone, _) {
        var webworkConfigModel = new webworkConfigModel.Model(JSON.parse($("#configJson").val()));
        var actionsView = new webworkConfigView.View({model : webworkConfigModel});

})