define('webwork/config/view', [
    'jquery',
    'backbone',
    'underscore'
], function($, Backbone, _) {
    "use strict";
  var AppView = Backbone.View.extend({
    events: {
    	 "click #config-save-button" : "saveConfig",
    	 "click #next-button" : "nextButton",
    	 "click #back-button" : "prevButton"

    },
    saveConfig: function(){
        this.model.set("parameter3", $("#parameter3").val());
        this.model.set("parameter4", $("#parameter4").val());
        $("#configJson").val(JSON.stringify(this.model));
    },
    nextButton: function(){
        this.model.set("parameter1", $("#parameter1").val());
        this.model.set("parameter2", $("#parameter2").val());
        var template = webwork.config.page2({configJson:$("#configJson").val(), parameter3:this.model.get('parameter3'), parameter4:this.model.get('parameter4')});
        $("#container").replaceWith(template);
        $("#configJson").val(JSON.stringify(this.model));
    },
    prevButton: function(){
            this.model.set("parameter3", $("#parameter3").val());
            this.model.set("parameter4", $("#parameter4").val());
            var template = webwork.config.page1({configJson:$("#configJson").val(), parameter1:this.model.get('parameter1'), parameter2:this.model.get('parameter2')});
            $("#container").replaceWith(template);
            $("#configJson").val(JSON.stringify(this.model));
        },
    initialize: function(){
          this.render();
    },
    render: function(){
          var template = webwork.config.page1({configJson:$("#configJson").val(), parameter1:this.model.get('parameter1'), parameter2:this.model.get('parameter2')});
          $("#container").replaceWith(template);
    },
    el: '#maincontainer'
  });
  return {
          View: AppView
  };
})